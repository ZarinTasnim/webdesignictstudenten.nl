<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/documentation/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'webdesignictstud_8' );

/** Database username */
define( 'DB_USER', 'webdesignictstud_8' );

/** Database password */
define( 'DB_PASSWORD', '@7p)h5SL7H' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'tdqtv0jobtlce3nx6scw8nsz2gv7in1l2k9wp9vbxy9zsx74spvpoczhs7geyubg' );
define( 'SECURE_AUTH_KEY',  'e7u2e75rnzwid7yzb9yoc3ousj4e7ubcgnbvzy2daxe1zo7sstxp0mdcmcgcx6ys' );
define( 'LOGGED_IN_KEY',    'l6nf0h3hojwfchekmo2mou51khhq79r0jj3dg6gemx9yzzlwtl2xlf83i8chojjw' );
define( 'NONCE_KEY',        'treg3kqjihgohjh46sj85ssfb4c4hnd0er5d0at11r4gzsqrtas5zhjqcdic4c5z' );
define( 'AUTH_SALT',        'l6zychktbhhewkvabw1bs17ovlr869fhcp6yjoqtuacyofom1mjdhwkuvyipsabj' );
define( 'SECURE_AUTH_SALT', 'gychzrjpggpfyq2pi8gxsdyyolwfdlaqivyxyrsr3uvlphzvy3i0ra6yz75xxprr' );
define( 'LOGGED_IN_SALT',   'g8fqnb2h34mbcufu0tewawsoafqj4tgwbnmhlfhk9kjynjqbybv6urmebwovqufd' );
define( 'NONCE_SALT',       'fdgyxbz9tbkjoclaqex6bhf0zrrx75kbpwahw7kbtrptcryhvvi8ukhmlfz3njho' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wpvf_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/documentation/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
