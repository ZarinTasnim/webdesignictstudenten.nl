<?php
/**
 * Plugin Name: Content Duplicator
 * Plugin URI: https://webdesign-studenten.nl
 * Description: A simple plugin to duplicate posts and pages.
 * Version: 1.0
 * Author: Zarin Tasnim
 * Author URI: https://webdesign-studenten.nl
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Duplicate Post Function
 */
function content_duplicator_duplicate_post() {
    // Check if the user has the capability to create posts
    if ( ! current_user_can( 'edit_posts' ) ) {
        wp_die( 'You do not have sufficient permissions to access this page.' );
    }

    // Verify the nonce for security
    check_admin_referer( 'duplicate_post_' . $_GET['post'] );

    // Get the original post
    $post_id = isset( $_GET['post'] ) ? absint( $_GET['post'] ) : '';
    $post = get_post( $post_id );

    // Clone the post and insert it
    if ( isset( $post ) && $post != null ) {
        $new_post = array(
            'post_title'    => $post->post_title . ' (Copy)',
            'post_content'  => $post->post_content,
            'post_status'   => 'draft',
            'post_type'     => $post->post_type,
            'post_author'   => get_current_user_id(),
        );

        $new_post_id = wp_insert_post( $new_post );

        // Redirect to the edit post screen for the new draft
        wp_redirect( admin_url( 'post.php?action=edit&post=' . $new_post_id ) );
        exit;
    } else {
        wp_die( 'Post creation failed, could not find original post.' );
    }
}

/**
 * Duplicate Post Link
 */
function content_duplicator_duplicate_post_link( $actions, $post ) {
    if ( current_user_can( 'edit_posts' ) ) {
        $actions['duplicate'] = '<a href="' . wp_nonce_url( 'admin.php?action=content_duplicator_duplicate_post&post=' . $post->ID, 'duplicate_post_' . $post->ID ) . '" title="Duplicate this item" rel="permalink">Duplicate</a>';
    }
    return $actions;
}

add_action( 'admin_action_content_duplicator_duplicate_post', 'content_duplicator_duplicate_post' );
add_filter( 'post_row_actions', 'content_duplicator_duplicate_post_link', 10, 2 );
add_filter( 'page_row_actions', 'content_duplicator_duplicate_post_link', 10, 2 );
